#include <cstdio>
#include <string>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <cstring>
#include <assert.h>

#include "wham-headers.h"

using namespace std;


int converged(CmdLineArgs& cmdLineArgs, CPUData& cpuData, GPUData& gpuData){

	int conv = 1;

	if (CONVERGENCE_STRATEGY == 1) {  // fast - breaks on first unconverged value
		float diff = 0;
		for (int i = 0; i < cmdLineArgs.H; i++) {
			diff = fabs(cpuData.h_Fnew[i] - cpuData.h_Fold[i]);
			if (diff >= cmdLineArgs.tol) {
				conv = -1;
				break;
			}
		}

	} else if (CONVERGENCE_STRATEGY == 2) { // works out error, lets me see if it diverges
		float diff = 0;
		float totalDiff = 0;
		for (int i = 0; i < cmdLineArgs.H; i++) {
			diff = fabs(cpuData.h_Fnew[i] - cpuData.h_Fold[i]);
			totalDiff += diff;
			if (diff >= cmdLineArgs.tol) {
				conv = -1;
			}
		}

		if (LOG) { printf("Average F difference: %f\n", totalDiff / (float)cmdLineArgs.H ); }

	} else if (CONVERGENCE_STRATEGY == 3) { // Works out error. Uses the ratio from Michelles thesis. Lets me see if it diverges
		float ratio = 0;
		float totalDiff = 0;
		for (int i = 0; i < cmdLineArgs.H; i++) {
			ratio = fabs(cpuData.h_Fnew[i] - cpuData.h_Fold[i]) / cpuData.h_Fnew[i];
			totalDiff += ratio;
			if (ratio >= cmdLineArgs.tol) {
				conv = -1;
			}
		}

		if (LOG) { printf("Average F / F - F-1 difference: %f\n", totalDiff / (float)cmdLineArgs.H ); }

	} else if (CONVERGENCE_STRATEGY == 4) { // Grossfield method

		float kT = k_B_KCal * cmdLineArgs.temp;

		float diff = 0;
		float totalDiff = 0;
		for (int i = 0; i < cmdLineArgs.H; i++) {
			diff = fabs((kT * log(cpuData.h_Fnew[i])) - (kT * log(cpuData.h_Fold[i])));
			totalDiff += diff;
			if (diff >= cmdLineArgs.tol) {
				conv = -1;
			}
		}

		if (LOG) { printf("Average F difference: %f\n", totalDiff / (float)cmdLineArgs.H ); }

	}


	return conv;
}
