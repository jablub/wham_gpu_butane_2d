#include <cstdio>
#include <string>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <cstring>
#include <assert.h>

#include "wham-headers.h"

#include <cuda_runtime.h>
#include <helper_functions.h>
#include <helper_cuda.h>

using namespace std;

void writePMF(CmdLineArgs& cmdLineArgs, CPUData& cpuData) {
	char* fileName = concat(cmdLineArgs.pmfOutDir, "/PMFP");
	if (LOG) printf("\nWriting : %s\n", fileName);
	FILE * file = fopen(fileName, "w");

	if (file == 0) {
		printf("Error. Cannot write to : %s\n", fileName);
		exit(-1);
	}

	int binIndexND[cmdLineArgs.D];
	float binCenterND[cmdLineArgs.D];


	for(int i = 0; i < cmdLineArgs.B; i++){
		raise(cmdLineArgs.D, cmdLineArgs.shape, i, binIndexND);
		binCenter(cmdLineArgs.D, cmdLineArgs.HBounds, cmdLineArgs.binWidths, binIndexND, binCenterND);
		for (int d = 0; d < cmdLineArgs.D; d++){
			fprintf(file, "%f ", binCenterND[d]);
		}
		fprintf(file, "%.10e %.10e\n", cpuData.h_PMF[i], cpuData.h_P[i]);
	}

	fclose (file);

}


void finalize(CmdLineArgs& cmdLineArgs, MetaData& metaData, CPUData& cpuData){

	free(cmdLineArgs.HBounds);
	free(cmdLineArgs.binWidths);
	free(metaData.sampleFiles);
	free(metaData.umbrellaCenters);
	free(metaData.springConstants);
	free(cpuData.h_H);
	free(cpuData.h_B);
	free(cpuData.h_Fnew);
	free(cpuData.h_Fold);
	free(cpuData.h_P);
	free(cpuData.h_PMF);
	free(cpuData.h_col_sum);
	free(cpuData.h_col_sum_inter);
	free(cpuData.h_debug);
	free(cpuData.h_row_sum_inter);

}

int main(int argc, char **argv) {

	if (TEST) {
		runTests();
		exit(0);
	}

	CmdLineArgs cmdLineArgs = CmdLineArgs();
	MetaData metaData = MetaData();
	CPUData cpuData = CPUData();
	GPUData gpuData = GPUData();
	WHAMTimes whamTimes = WHAMTimes();


		// RUN WHAM

		StopWatchInterface *total_timer = NULL;
		sdkCreateTimer(&total_timer);
		sdkStartTimer(&total_timer);

		StopWatchInterface *hb_timer = NULL;
		sdkCreateTimer(&hb_timer);
		StopWatchInterface *c_timer = NULL;
		sdkCreateTimer(&c_timer);
		StopWatchInterface *wham_timer = NULL;
		sdkCreateTimer(&wham_timer);

		printCmdLineArgs(argc, argv);

		parseCommandLine(cmdLineArgs, argc, argv);
		init(metaData, cmdLineArgs.D, cmdLineArgs.H);
		parseMetadataFile(cmdLineArgs, metaData);

		init(cpuData, cmdLineArgs.H, cmdLineArgs.B);
		init(gpuData, cmdLineArgs.H, cmdLineArgs.B);

		sdkStartTimer(&hb_timer);
		createHistograms(gpuData, cmdLineArgs, metaData, cpuData, whamTimes);
		sdkStopTimer(&hb_timer);
		whamTimes.hb_time += sdkGetTimerValue(&hb_timer);

		sdkStartTimer(&c_timer);
		calculateDistributionBiases(gpuData, cmdLineArgs, metaData, whamTimes);
		sdkStopTimer(&c_timer);
		whamTimes.c_time += sdkGetTimerValue(&c_timer);

		if (DEBUG) {
			float* h_C = (float*) malloc(gpuData.H_resized * gpuData.B_resized * sizeof(float));
			checkCudaErrors(cudaMemcpy(h_C, gpuData.d_C, gpuData.H_resized * gpuData.B_resized * sizeof(float), cudaMemcpyDeviceToHost));
			fpprn(concat(cmdLineArgs.pmfOutDir, "/C"), h_C, 1, gpuData.H_resized * gpuData.B_resized);
		}

		sdkStartTimer(&wham_timer);
		whamGPU(cmdLineArgs, cpuData, gpuData, whamTimes, false);
		sdkStopTimer(&wham_timer);
		whamTimes.wham_time += sdkGetTimerValue(&wham_timer);

		writePMF(cmdLineArgs, cpuData);

		sdkStopTimer(&total_timer);
		whamTimes.total_time += sdkGetTimerValue(&total_timer);

		if (LOG) pprn("P", cpuData.h_P, 1, 10);
		if (LOG) pprn("PMF", cpuData.h_PMF, 1, 10);

		if (LOG) fpprn(concat(cmdLineArgs.pmfOutDir, "/P"), cpuData.h_P, 1, cmdLineArgs.B);
		if (LOG) fpprn(concat(cmdLineArgs.pmfOutDir, "/PMF"), cpuData.h_PMF, 1, cmdLineArgs.B);

		finalize(cmdLineArgs, metaData, cpuData);


	if (TIME) {
		char* fileName = concat(cmdLineArgs.pmfOutDir, "/T");
		if (LOG) printf("\nWriting : %s\n", fileName);
		FILE * file = fopen(fileName, "w");

		if (file == 0) {
			printf("Error. Cannot write to : %s\n", fileName);
			exit(-1);
		}
		fprintf(file, "{\n:total-time %f\n:c-time %f\n:hb-time %f\n:wham-time %f\n:no-of-iterations %d\n}", whamTimes.total_time, whamTimes.c_time, whamTimes.hb_time, whamTimes.wham_time, whamTimes.no_of_iterations);
		if (LOG) printf(":total-time %f\n:c-time %f\n:hb-time %f\n:wham-time %f\n:no-of-iterations %d\n", whamTimes.total_time, whamTimes.c_time, whamTimes.hb_time, whamTimes.wham_time, whamTimes.no_of_iterations);
		fclose (file);
	}

	if (LOG) printf("\nfin.");

}
