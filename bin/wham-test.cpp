#include <cstdio>
#include <string>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <cstring>
#include <assert.h>

#include "wham-headers.h"

// CUDA
#include <cuda_runtime.h>
#include <helper_functions.h>
#include <helper_cuda.h>

using namespace std;



void create1DcmdLineValues(char** argv) {
	argv[0 ] = "/media/andrew/wham/wham/dev/cuda/wham-cuda-paper/Debug/wham-cuda-paper";
	argv[1 ] = "1";
	argv[2 ] = "7";
	argv[3 ] = "0.001";
	argv[4 ] = "300";
	argv[5 ] = "76";
	argv[6 ] = "0.5";
	argv[7 ] = "/media/andrew/wham/wham/data/toy-data-thesis/metadata";
	argv[8 ] = "1";
	argv[9 ] = "/media/andrew/wham/wham/data/toy-data-thesis/B20";
	argv[10] = "-180";
	argv[11] = "180";
	argv[12] = "20";
}

void create2DcmdLineValues(char** argv) {
	argv[0 ] = "/media/andrew/wham/wham/dev/cuda/wham-cuda-paper/Debug/wham-cuda-paper";
	argv[1 ] = "2";
	argv[2 ] = "361";
	argv[3 ] = "0.001";
	argv[4 ] = "300";
	argv[5 ] = "1000";
	argv[6 ] = "0.5";
	argv[7 ] = "/media/andrew/wham/wham/data/WHAM_GPU-H19x19-K0.001x0.001-S50000-ns0.5x0.5/metadata";
	argv[8 ] = "1";
	argv[9 ] = "/media/andrew/wham/wham/data/WHAM_GPU-H19x19-K0.001x0.001-S50000-ns0.5x0.5/B30x30";
	argv[10] = "-180";
	argv[11] = "180";
	argv[12] = "30";
	argv[13] = "-180";
	argv[14] = "180";
	argv[15] = "30";
}

void create3DcmdLineValues(char** argv) {
	argv[0 ] = "/media/andrew/wham/wham/dev/cuda/wham-cuda-paper/Debug/wham-cuda-paper";
	argv[1 ] = "3";
	argv[2 ] = "31";
	argv[3 ] = "0.001";
	argv[4 ] = "300";
	argv[5 ] = "10000";
	argv[6 ] = "0.25";
	argv[7 ] = "/media/andrew/wham/wham/data/WHAM_GPU_WEN_3D/metadata";
	argv[8 ] = "0";
	argv[9 ] = "/media/andrew/wham/wham/data/WHAM_GPU_WEN_3D/B40x40x40";
	argv[10] = "0.856756448745728";
	argv[11] = "2.90161752700806";
	argv[12] = "40";
	argv[13] = "-1.34243750572205";
	argv[14] = "2.32785224914551";
	argv[15] = "40";
	argv[16] = "-1.70684254169464";
	argv[17] = "3.74990844726562";
	argv[18] = "40";

}

// utils
void strtostrTest(){
	if (LOG) printf(">-- strtostrTest\n");

	char* str = "WHAT THE HEY!!";
	assert(strcmp("WHAT" , strtostr(str, &str)) == 0);
	assert(strcmp("THE" , strtostr(str, &str)) == 0);
	assert(strcmp("HEY!!" , strtostr(str, &str)) == 0);

	char* str2 = "/media/andrew/wham/wham/data/H19x19-K0.001x0.001-S50000-ns0.5x0.5/prod_-180_-140 -180 -140 0.002 0.002\n";
	if (LOG) printf("strtostrTest : -[%s]-\n",strtostr(str2, &str2) );

}

// parse
void parseCommandLineTest(){
	if (LOG) printf(">-- parseCommandLineTest\n");
	// command line params
	int argc = 16;
	char **argv = (char**)malloc(sizeof(char*) * argc);
	if(argv == NULL) {printf("error"); exit(1);}

	create2DcmdLineValues(argv);
	CmdLineArgs cmdLineArgs = CmdLineArgs();
	if (LOG) printCmdLineArgs(argc, argv);
	parseCommandLine(cmdLineArgs, argc, argv);
	if (LOG) pprn("HBounds", cmdLineArgs.HBounds, 2, cmdLineArgs.D);
	if (LOG) pprn("binWidths", cmdLineArgs.binWidths, 2, 1);
	if (LOG) pprn("shape", cmdLineArgs.shape, 2, 1);

	assert(cmdLineArgs.B == 900);
	assert(cmdLineArgs.binWidths[0] == 12.0);
	assert(cmdLineArgs.binWidths[1] == 12.0);

	free(argv);

}

// TODO write a metadata file unit test

// histograms
void binIndexNDTest() {
	if (LOG) printf(">-- binIndexNDTest\n");

	int D = 3;
	float Hbounds[6] = {-5, 5, -3, 3, -4, 4};
	float binWidths[3] = {1,1,1};
	float point[3] = {-1, -1, 2};
	int binIndex[3];

	binIndexND(D, Hbounds, binWidths, point, binIndex);

	if (LOG) pprn("binIndex", binIndex, D, 1);
	assert(4 == binIndex[0]);
	assert(2 == binIndex[1]);
	assert(6 == binIndex[2]);


}

void flattenNDTest() {
	if (LOG) printf(">-- flattenNDTest\n");

	int flatIndex;

	// 1, 2 and 3 D tests
	int shape[3] = {8, 6, 4};
	int ndIndex[3] = {4, 3, 2};

	flatIndex = flattenND(1, shape, ndIndex);
	assert(4 == flatIndex);

	flatIndex = flattenND(2, shape, ndIndex);
	assert(28 == flatIndex);

	flatIndex = flattenND(3, shape, ndIndex);
	assert(124 == flatIndex);

}

// distribution biases
void calculateDistributionBiases1DTest(){
	if (LOG) printf(">-- calculateDistributionBiases1DTest\n");

	CmdLineArgs cmdLineArgs = CmdLineArgs();
	MetaData metaData = MetaData();
	GPUData gpuData = GPUData();
	WHAMTimes whamTimes = WHAMTimes();

	cmdLineArgs.D = 1;
	cmdLineArgs.H = 7;
	cmdLineArgs.B = 20;
	cmdLineArgs.temp = 300;
	cmdLineArgs.spring_constant_modifier = 1;
	init(gpuData, cmdLineArgs.H, cmdLineArgs.B);

	int _shape[] = {20};
	cmdLineArgs.shape = _shape;

	float _hb[] = {-180.0, 180.0};
	cmdLineArgs.HBounds = _hb;

	float _bw[] = {18.0};
	cmdLineArgs.binWidths = _bw;

	float _uc[] = {-180.0, -120.0, -60.0, 0.0, 60.0, 120.0, 180.0};
	metaData.umbrellaCenters = _uc;

	float _k[] = {0.0005, 0.0005, 0.0005, 0.0005, 0.0005, 0.0005, 0.0005};
	metaData.springConstants = _k;

	calculateDistributionBiases(gpuData, cmdLineArgs, metaData, whamTimes);

	// transfer the values back from the GPU
	float* h_C = (float*) malloc(gpuData.H_resized * gpuData.B_resized * sizeof(float));
	checkCudaErrors(cudaMemcpy(h_C, gpuData.d_C, gpuData.H_resized * gpuData.B_resized * sizeof(float), cudaMemcpyDeviceToHost));

	// finalize
	checkCudaErrors(cudaFree(gpuData.d_C));

	if (LOG) pprn("C", h_C, gpuData.B_resized, gpuData.H_resized);

	// Results
	//	   0.93418   0.54187   0.18231   0.03558   0.00403   0.00026   0.00001   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
	//	   0.11235   0.40039   0.82769   0.99246   0.69028   0.27848   0.06517   0.00885   0.00070   0.00003   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
	//	   0.00003   0.00070   0.00885   0.06517   0.27848   0.69028   0.99246   0.82769   0.40039   0.11235   0.01829   0.00173   0.00009   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
	//	   0.00000   0.00000   0.00000   0.00001   0.00026   0.00403   0.03558   0.18231   0.54187   0.93418   0.93418   0.54187   0.18231   0.03558   0.00403   0.00026   0.00001   0.00000   0.00000   0.00000
	//	   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00009   0.00173   0.01829   0.11235   0.40039   0.82769   0.99246   0.69028   0.27848   0.06517   0.00885   0.00070   0.00003
	//	   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00003   0.00070   0.00885   0.06517   0.27848   0.69028   0.99246   0.82769   0.40039   0.11235
	//	   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00001   0.00026   0.00403   0.03558   0.18231   0.54187   0.93418
	assert(abs(h_C[0] - 0.93418) < 0.0001);
	assert(abs(h_C[1] - 0.54187) < 0.0001);

}

void calculateDistributionBiases2DTest(){
	if (LOG) printf(">-- calculateDistributionBiases2DTest\n");

	CmdLineArgs cmdLineArgs = CmdLineArgs();
	MetaData metaData = MetaData();
	GPUData gpuData = GPUData();
	WHAMTimes whamTimes = WHAMTimes();


	cmdLineArgs.D = 2;
	cmdLineArgs.H = 1;
	cmdLineArgs.B = 18;
	cmdLineArgs.temp = 300;
	cmdLineArgs.spring_constant_modifier = 1;
	int D = cmdLineArgs.D;
	int H = cmdLineArgs.H;
	int B = cmdLineArgs.B;
	init(gpuData, cmdLineArgs.H, cmdLineArgs.B);

	int _shape[] = {3, 6};
	cmdLineArgs.shape = _shape;

	float _hb[] = {0.0, 3.0, -6.0, 6.0};
	cmdLineArgs.HBounds = _hb;

	float _bw[] = {1.0, 2.0};
	cmdLineArgs.binWidths = _bw;

	float _uc[] = {0, -6};
	metaData.umbrellaCenters = _uc;

	float _k[] = {0.001, 0.001};
	metaData.springConstants = _k;

	calculateDistributionBiases(gpuData, cmdLineArgs, metaData, whamTimes);

	// transfer the values back from the GPU
	float* h_C = (float*) malloc(gpuData.H_resized * gpuData.B_resized * sizeof(float));
	checkCudaErrors(cudaMemcpy(h_C, gpuData.d_C, gpuData.H_resized * gpuData.B_resized * sizeof(float), cudaMemcpyDeviceToHost));

	// finalize
	checkCudaErrors(cudaFree(gpuData.d_C));

	if (LOG) pprn("C", h_C, gpuData.B_resized, gpuData.H_resized);

	// Results
	//	   0.99790   0.98457   0.95844   0.92054   0.87233   0.81561
	//	   0.99455   0.98127   0.95523   0.91745   0.86941   0.81287
	//	   0.98789   0.97469   0.94882   0.91131   0.86358   0.80742
	assert(abs(h_C[0] - 0.997901) < 0.0001);
	assert(abs(h_C[1] - 0.984571) < 0.0001);

}

void calculateDistributionBiases3DTest(){
	if (LOG) printf(">-- calculateDistributionBiases3DTest\n");

	CmdLineArgs cmdLineArgs = CmdLineArgs();
	MetaData metaData = MetaData();
	GPUData gpuData = GPUData();
	WHAMTimes whamTimes = WHAMTimes();


	cmdLineArgs.D = 3;
	cmdLineArgs.H = 1;
	cmdLineArgs.B = 5*5*5;
	cmdLineArgs.temp = 300;
	cmdLineArgs.spring_constant_modifier = 1;
	int D = cmdLineArgs.D;
	int H = cmdLineArgs.H;
	int B = cmdLineArgs.B;
	init(gpuData, cmdLineArgs.H, cmdLineArgs.B);

	int _shape[] = {5, 5, 5};
	cmdLineArgs.shape = _shape;

	float _hb[] = {1, 3, -1, 3, -2, 4};
	cmdLineArgs.HBounds = _hb;

	float _bw[] = {0.4, 0.8, 1.2};
	cmdLineArgs.binWidths = _bw;

	float _uc[] = {1, -1, -2};
	metaData.umbrellaCenters = _uc;

	float _k[] = {10, 10, 10};
	metaData.springConstants = _k;

	calculateDistributionBiases(gpuData, cmdLineArgs, metaData, whamTimes);

	// transfer the values back from the GPU
	float* h_C = (float*) malloc(gpuData.H_resized * gpuData.B_resized * sizeof(float));
	checkCudaErrors(cudaMemcpy(h_C, gpuData.d_C, gpuData.H_resized * gpuData.B_resized * sizeof(float), cudaMemcpyDeviceToHost));

	if (LOG) pprn("C", h_C, gpuData.B_resized, gpuData.H_resized);
	fpprn(concat("/tmp", "/C"), h_C, 1, gpuData.H_resized * gpuData.B_resized);

	// finalize
	checkCudaErrors(cudaFree(gpuData.d_C));

	// Results
	assert(abs(h_C[0] - 0.0000815) < 0.0000001);

}


void createHistograms1DTest(){
	if (LOG) printf(">-- createHistograms1DTest\n");


	CmdLineArgs cmdLineArgs = CmdLineArgs();
	MetaData metaData = MetaData();
	CPUData cpuData = CPUData();
	GPUData gpuData = GPUData();
	WHAMTimes whamTimes = WHAMTimes();


	cmdLineArgs.D = 1;
	cmdLineArgs.H = 7;
	cmdLineArgs.B = 20;
	cmdLineArgs.temp = 300;
	cmdLineArgs.spring_constant_modifier = 0.5;

	init(gpuData, cmdLineArgs.H, cmdLineArgs.B);

	int _shape[] = {20};
	cmdLineArgs.shape = _shape;

	float _hb[] = {-180.0, 180.0};
	cmdLineArgs.HBounds = _hb;

	float _bw[] = {18.0};
	cmdLineArgs.binWidths = _bw;

	float _uc[] = {-180.0, -120.0, -60.0, 0.0, 60.0, 120.0, 180.0};
	metaData.umbrellaCenters = _uc;

	float _k[] = {0.0005, 0.0005, 0.0005, 0.0005, 0.0005, 0.0005, 0.0005};
	metaData.springConstants = _k;

	metaData.sampleFiles = (char**)malloc(sizeof(char*) * cmdLineArgs.H);
	metaData.sampleFiles[0] = "/media/andrew/wham/wham/data/toy-data-thesis/toy_-180";
	metaData.sampleFiles[1] = "/media/andrew/wham/wham/data/toy-data-thesis/toy_-120";
	metaData.sampleFiles[2] = "/media/andrew/wham/wham/data/toy-data-thesis/toy_-60";
	metaData.sampleFiles[3] = "/media/andrew/wham/wham/data/toy-data-thesis/toy_0";
	metaData.sampleFiles[4] = "/media/andrew/wham/wham/data/toy-data-thesis/toy_60";
	metaData.sampleFiles[5] = "/media/andrew/wham/wham/data/toy-data-thesis/toy_120";
	metaData.sampleFiles[6] = "/media/andrew/wham/wham/data/toy-data-thesis/toy_180";
	cmdLineArgs.skipFirst = 1;

	init(cpuData, cmdLineArgs.H, cmdLineArgs.B);

	createHistograms(gpuData, cmdLineArgs, metaData, cpuData, whamTimes);

	if (LOG) pprn("CPU H", cpuData.h_H, 7, 1);
	if (LOG) pprn("CPU B", cpuData.h_B, 20, 1);
	assert(7400 == cpuData.h_B[0]);
	assert(2073 == cpuData.h_B[1]);
	assert(252 == cpuData.h_B[2]);
	assert(46 == cpuData.h_B[3]);

	// transfer the values back from the GPU
	int* h_H = (int*) malloc(gpuData.H_resized * sizeof(int));
	checkCudaErrors(cudaMemcpy(h_H, gpuData.d_H, gpuData.H_resized * sizeof(int), cudaMemcpyDeviceToHost));
	if (LOG) pprn("GPU H", h_H, gpuData.H_resized, 1);

	int* h_B = (int*) malloc(gpuData.B_resized * sizeof(int));
	checkCudaErrors(cudaMemcpy(h_B, gpuData.d_B, gpuData.B_resized * sizeof(int), cudaMemcpyDeviceToHost));
	if (LOG) pprn("GPU B", h_B, gpuData.B_resized, 1);

	// finalize
	checkCudaErrors(cudaFree(gpuData.d_H));
	checkCudaErrors(cudaFree(gpuData.d_B));

	assert(5000 == cpuData.h_H[0]);
	assert(4997 == cpuData.h_H[cmdLineArgs.H - 1]);
	assert(7373 == cpuData.h_B[cmdLineArgs.B - 1]);

	assert(7400 == h_B[0]);
	assert(2073 == h_B[1]);
	assert(252 == h_B[2]);
	assert(46 == h_B[3]);
}


void endToEnd1D(){
	if (LOG) printf(">-- endToEnd1D\n");


	// command line params
	int argc = 13;
	char **argv = (char**)malloc(sizeof(char*) * argc);

	CmdLineArgs cmdLineArgs = CmdLineArgs();
	MetaData metaData = MetaData();
	CPUData cpuData = CPUData();
	GPUData gpuData = GPUData();
	WHAMTimes whamTimes = WHAMTimes();

	create1DcmdLineValues(argv);
	parseCommandLine(cmdLineArgs, argc, argv);

	init(metaData, cmdLineArgs.D, cmdLineArgs.H);
	parseMetadataFile(cmdLineArgs, metaData);
	assert(-180.0 == metaData.umbrellaCenters[0]);
	assert(-120.0 == metaData.umbrellaCenters[1]);
	assert(abs(metaData.springConstants[0] - 0.0005) < 0.0001);

	init(cpuData, cmdLineArgs.H, cmdLineArgs.B);
	init(gpuData, cmdLineArgs.H, cmdLineArgs.B);

	createHistograms(gpuData, cmdLineArgs, metaData, cpuData, whamTimes);

	if (LOG) pprn("CPU H", cpuData.h_H, 7, 1);
	if (LOG) pprn("CPU B", cpuData.h_B, 20, 1);

	assert(5000 == cpuData.h_H[0]);
	assert(4997 == cpuData.h_H[cmdLineArgs.H - 1]);
	assert(7400 == cpuData.h_B[0]);
	assert(7373 == cpuData.h_B[cmdLineArgs.B - 1]);

	calculateDistributionBiases(gpuData, cmdLineArgs, metaData, whamTimes);

	// transfer the values back from the GPU
	float* h_C = (float*) malloc(gpuData.H_resized * gpuData.B_resized * sizeof(float));
	checkCudaErrors(cudaMemcpy(h_C, gpuData.d_C, gpuData.H_resized * gpuData.B_resized * sizeof(float), cudaMemcpyDeviceToHost));

	if (LOG) pprn("C", h_C, gpuData.B_resized, gpuData.H_resized);
	assert(abs(h_C[0] - 0.93418) < 0.0001);
	assert(abs(h_C[1] - 0.54187) < 0.0001);

	whamGPU(cmdLineArgs, cpuData, gpuData, whamTimes, false);

	fpprn(concat(cmdLineArgs.pmfOutDir, "/P"), cpuData.h_P, 1, cmdLineArgs.B);
	fpprn(concat(cmdLineArgs.pmfOutDir, "/PMF"), cpuData.h_PMF, 1, cmdLineArgs.B);

	writePMF(cmdLineArgs, cpuData);

	finalize(cmdLineArgs, metaData, cpuData);

}

void endToEnd2D(){
	if (LOG) printf(">-- endToEnd2D\n");

	// command line params
	int argc = 16;
	char **argv = (char**)malloc(sizeof(char*) * argc);

	CmdLineArgs cmdLineArgs = CmdLineArgs();
	MetaData metaData = MetaData();
	CPUData cpuData = CPUData();
	GPUData gpuData = GPUData();
	WHAMTimes whamTimes = WHAMTimes();


	create2DcmdLineValues(argv);
	parseCommandLine(cmdLineArgs, argc, argv);
	printCmdLineArgs(argc, argv);

	init(metaData, cmdLineArgs.D, cmdLineArgs.H);
	parseMetadataFile(cmdLineArgs, metaData);
	assert(-180.0 == metaData.umbrellaCenters[0]);
	assert(-180.0 == metaData.umbrellaCenters[1]);
	assert(abs(metaData.springConstants[0] - 0.0005) < 0.0001);
	assert(abs(metaData.springConstants[1] - 0.0005) < 0.0001);

	init(cpuData, cmdLineArgs.H, cmdLineArgs.B);
	init(gpuData, cmdLineArgs.H, cmdLineArgs.B);

	createHistograms(gpuData, cmdLineArgs, metaData, cpuData, whamTimes);

	if (LOG) pprn("H", cpuData.h_H, 19, 19);
	if (LOG) pprn("B", cpuData.h_B, 30, 30);

	assert(50000 == cpuData.h_H[0]);
	assert(50000 == cpuData.h_H[cmdLineArgs.H - 1]);
	assert(45705 == cpuData.h_B[0]);
	assert(45723 == cpuData.h_B[cmdLineArgs.B - 1]);

	calculateDistributionBiases(gpuData, cmdLineArgs, metaData, whamTimes);

	// transfer the values back from the GPU
	float* h_C = (float*) malloc(gpuData.H_resized * gpuData.B_resized * sizeof(float));
	checkCudaErrors(cudaMemcpy(h_C, gpuData.d_C, gpuData.H_resized * gpuData.B_resized * sizeof(float), cudaMemcpyDeviceToHost));
	fpprn(concat(cmdLineArgs.pmfOutDir, "/C"), h_C, 1, gpuData.H_resized * gpuData.B_resized);

	assert(abs(h_C[0] - 0.94127) < 0.0001);
	assert(abs(h_C[1] - 0.73890) < 0.0001);

	whamGPU(cmdLineArgs, cpuData, gpuData, whamTimes, false);
	if (LOG) pprn("P", cpuData.h_P, 1, 900);
	if (LOG) pprn("PMF", cpuData.h_PMF, 1, 900);

	writePMF(cmdLineArgs, cpuData);

	finalize(cmdLineArgs, metaData, cpuData);

}


void endToEnd3D(){
	if (LOG) printf(">-- endToEnd3D\n");

	// command line params
	int argc = 20;
	char **argv = (char**)malloc(sizeof(char*) * argc);

	CmdLineArgs cmdLineArgs = CmdLineArgs();
	MetaData metaData = MetaData();
	CPUData cpuData = CPUData();
	GPUData gpuData = GPUData();
	WHAMTimes whamTimes = WHAMTimes();


	create3DcmdLineValues(argv);
	parseCommandLine(cmdLineArgs, argc, argv);
	printCmdLineArgs(argc, argv);

	init(metaData, cmdLineArgs.D, cmdLineArgs.H);
	parseMetadataFile(cmdLineArgs, metaData);
	init(cpuData, cmdLineArgs.H, cmdLineArgs.B);
	init(gpuData, cmdLineArgs.H, cmdLineArgs.B);
	createHistograms(gpuData, cmdLineArgs, metaData, cpuData, whamTimes);
	fpprn(concat(cmdLineArgs.pmfOutDir, "/H"), cpuData.h_H, 1, cmdLineArgs.H);
	if (LOG) printf("Sum H %d\n", sum(cpuData.h_H, cmdLineArgs.H));
	fpprn(concat(cmdLineArgs.pmfOutDir, "/B"), cpuData.h_B, 1, cmdLineArgs.B);
	if (LOG) printf("Sum B %d\n", sum(cpuData.h_B, cmdLineArgs.B));
	calculateDistributionBiases(gpuData, cmdLineArgs, metaData, whamTimes);

	// transfer the values back from the GPU and dump to file
	float* h_C = (float*) malloc(gpuData.H_resized * gpuData.B_resized * sizeof(float));
	checkCudaErrors(cudaMemcpy(h_C, gpuData.d_C, gpuData.H_resized * gpuData.B_resized * sizeof(float), cudaMemcpyDeviceToHost));
	fpprn(concat(cmdLineArgs.pmfOutDir, "/C"), h_C, 1, gpuData.H_resized * gpuData.B_resized);

	whamGPU(cmdLineArgs, cpuData, gpuData, whamTimes, false);

	fpprn(concat(cmdLineArgs.pmfOutDir, "/P"), cpuData.h_P, 1, gpuData.B_resized);
	fpprn(concat(cmdLineArgs.pmfOutDir, "/PMF"), cpuData.h_PMF, 1, gpuData.B_resized);

	finalize(cmdLineArgs, metaData, cpuData);

}


bool runTests(){

	if (LOG) printf(">>>---\n");

	strtostrTest();
	parseCommandLineTest();
	flattenNDTest();
	calculateDistributionBiases1DTest();
	calculateDistributionBiases2DTest();
	calculateDistributionBiases3DTest();
	binIndexNDTest();
	createHistograms1DTest();
	endToEnd1D();
//	endToEnd2D();  // XXX 1D and 2D together fail. Separately they are ok. WTF? It is a malloc error somewhere.
//	endToEnd3D();

	if (LOG) printf("\n>>>---\n");

	if (LOG) printf("ALL TESTS PASS!!\n");
	return true;
}


