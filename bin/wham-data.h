#define k_B_KCal 0.001982923700 // Boltzmann's constant in kcal/mol K

// XXX may be better to shift most of these to other places
struct CmdLineArgs {
	int D;
	int H;
	int B;
	float tol;
	float temp;
	int max_no_of_iterations;
	float spring_constant_modifier;
	char* metadataFile;
	int   skipFirst;
	char* pmfOutDir;
	int* shape;
	float* HBounds;
	float* binWidths;
};

struct MetaData {
	char** sampleFiles;
	float* umbrellaCenters;
	float* springConstants;

};

struct CPUData {
	int* h_H;
	int* h_B;
	float* h_col_sum_inter;
	float* h_col_sum;
	float* h_row_sum_inter;
	float* h_Fold;
	float* h_Fnew;
	float* h_P;
	float* h_PMF;

	float* h_debug;

};

struct GPUData {
	int blksX;
	int blksY;

	float * d_C;
	int* d_H;
	int H_resized;
	int* d_B;
	int B_resized;
	float* d_Fold;
	float* d_Fnew;
	float* d_row_sum_inter;
	float* d_col_sum_inter;
	float* d_P;
	float* d_PMF;

	float* d_debug;

	int no_of_iterations;
};


struct WHAMTimes {

	unsigned int problem_size;

	float hb_time = 0.0;
	float c_time = 0.0;
	float wham_time = 0.0;
	float total_time = 0.0;

	int no_of_iterations;

};





