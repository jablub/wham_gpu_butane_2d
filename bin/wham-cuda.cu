#include <cstdio>
#include <string>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <sstream>

#include "wham-headers.h"

// CUDA
#include <cuda_runtime.h>
#include <helper_functions.h>
#include <helper_cuda.h>

using namespace std;

static dim3 dimBlock, dimGrid;

void initCPU(CmdLineArgs& cmdLineArgs, CPUData& cpuData, GPUData& gpuData, bool debug) {
	cpuData.h_col_sum_inter = (float*) malloc(gpuData.blksY * gpuData.B_resized * sizeof(float));
	cpuData.h_col_sum = (float*) malloc(gpuData.B_resized * sizeof(float));
	cpuData.h_row_sum_inter = (float*) malloc(gpuData.blksX * gpuData.H_resized * sizeof(float));

	cpuData.h_Fold = (float*) malloc(gpuData.H_resized * sizeof(float));
	for (int i=0; i<gpuData.H_resized; i++) {cpuData.h_Fold[i] = 1.7;}

	cpuData.h_Fnew = (float*) malloc(gpuData.H_resized * sizeof(float));
	for (int i=0; i<gpuData.H_resized; i++) {cpuData.h_Fnew[i] = 12.7;}
	cpuData.h_P = (float*) malloc(gpuData.B_resized * sizeof(float));
	cpuData.h_PMF = (float*) malloc(gpuData.B_resized * sizeof(float));

	cpuData.h_debug = (float*) malloc(gpuData.H_resized * gpuData.B_resized * sizeof(float));

	if(cpuData.h_col_sum_inter == NULL) {printf("Error allocating h_col_sum_inter"); exit(1);}
	if(cpuData.h_col_sum == NULL) {printf("Error allocating h_col_sum"); exit(1);}
	if(cpuData.h_row_sum_inter == NULL) {printf("Error allocating h_row_sum_inter"); exit(1);}
	if(cpuData.h_Fold == NULL) {printf("Error allocating h_Fold"); exit(1);}
	if(cpuData.h_Fnew == NULL) {printf("Error allocating h_Fnew"); exit(1);}
	if(cpuData.h_P == NULL) {printf("Error allocating h_P"); exit(1);}
	if(cpuData.h_PMF == NULL) {printf("Error allocating h_PMF"); exit(1);}
	if(cpuData.h_debug == NULL) {printf("Error allocating h_debug"); exit(1);}


}

void init(GPUData& gpuData, int H, int B){

	gpuData.no_of_iterations = 0;

	// Create sizes padded to blocksize
	gpuData.H_resized = ceil((float)H / (float)BLOCKSIZE) * BLOCKSIZE;
	gpuData.B_resized = ceil((float)B / (float)BLOCKSIZE) * BLOCKSIZE;

	gpuData.blksY = gpuData.H_resized / BLOCKSIZE;
	gpuData.blksX = gpuData.B_resized / BLOCKSIZE;

	if (LOG) printf("Block size : %d\nResizing from : %dx%d to %dx%d\n", BLOCKSIZE, H, B, gpuData.H_resized, gpuData.B_resized);
}


static __global__ void dInit(float* d_Fold, float* d_Fnew, float* d_col_sum_inter, float* d_row_sum_inter, float* d_debug) {

	unsigned int d_tx = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int d_ty = blockDim.y * blockIdx.y + threadIdx.y;
	unsigned int d_size_x = gridDim.x * blockDim.x;
	unsigned int d_x_y_flat = d_ty * d_size_x + d_tx;

	d_col_sum_inter[d_size_x * blockIdx.y + d_tx] = 0.0;
	d_row_sum_inter[d_ty * gridDim.x + blockIdx.x] = 0.0;

	d_Fold[d_ty] = 1.;
	d_Fnew[d_ty] = 0.0;

	d_debug[d_x_y_flat] = 0.0;

}

void initGPU(CmdLineArgs& cmdLineArgs, CPUData& cpuData, GPUData& gpuData,  bool debug) {

	// malloc those muthas
	checkCudaErrors(cudaMalloc((void ** ) &gpuData.d_Fold, gpuData.H_resized * sizeof(float)));
	checkCudaErrors(cudaMalloc((void ** ) &gpuData.d_Fnew, gpuData.H_resized * sizeof(float)));
	checkCudaErrors(cudaMalloc((void ** ) &gpuData.d_P, gpuData.B_resized * sizeof(float)));
	checkCudaErrors(cudaMalloc((void ** ) &gpuData.d_PMF, gpuData.B_resized * sizeof(float)));
	checkCudaErrors(cudaMalloc((void ** ) &gpuData.d_col_sum_inter, gpuData.blksY * gpuData.B_resized * sizeof(float)));
	checkCudaErrors(cudaMalloc((void ** ) &gpuData.d_row_sum_inter, gpuData.blksX * gpuData.H_resized * sizeof(float)));
	checkCudaErrors(cudaMalloc((void ** ) &gpuData.d_debug, gpuData.H_resized * gpuData.B_resized * sizeof(float)));

	// Initialise grid
	dimBlock.x = BLOCKSIZE;
	dimBlock.y = BLOCKSIZE;
	dimGrid.x = gpuData.blksX;
	dimGrid.y = gpuData.blksY;
	if (LOG) printf("CPU Problem size:  H %d x B %d\n", cmdLineArgs.H, cmdLineArgs.B);
	if (LOG) printf("Grid setup: %d x %d blocks :: %d * %d threads \n", dimGrid.y, dimGrid.x, dimBlock.y, dimBlock.x);
	if (LOG) printf("GPU Problem size:  H %d x B %d\n", gpuData.H_resized, gpuData.B_resized);

	// init
	dInit<<<dimGrid, dimBlock>>>(gpuData.d_Fold, gpuData.d_Fnew, gpuData.d_col_sum_inter, gpuData.d_row_sum_inter, gpuData.d_debug);

}

static __global__ void dColSum(int* d_H, float* d_C, float* d_Fold, float* d_col_sum_inter, float* d_debug) {

	// get indecies
	unsigned int s_tx = threadIdx.x;
	unsigned int d_tx = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int d_size_x = gridDim.x * blockDim.x;

	unsigned int s_ty = threadIdx.y;
	unsigned int d_ty = blockDim.y * blockIdx.y + threadIdx.y;
//	unsigned int d_size_y = gridDim.y * blockSize;

	unsigned int d_x_y_flat = d_ty * d_size_x + d_tx;

	// Create shared mem NFC
	__shared__ float s_NFC[BLOCKSIZE][BLOCKSIZE];

	// Calculate NFC
	// @@ 14 ms  - including _sync (10ms if pre-cached version NHC used)
	s_NFC[s_ty][s_tx] = d_H[d_ty] * d_Fold[d_ty] * d_C[d_x_y_flat];

	__syncthreads();

	// Sum reduce each column of NFC
	// @@ 16ms
	for (unsigned int stride = blockDim.y / 2; stride >= 1; stride >>= 1) {
		__syncthreads();
		if (s_ty < stride) {
			s_NFC[s_ty][s_tx] += s_NFC[s_ty + stride][s_tx];
		}
	}
	__syncthreads();

	// Save row 0 to global mem
	// @@ 4 ms - strange they add up to more than total
	if (s_ty == 0) {
		d_col_sum_inter[d_size_x * blockIdx.y + d_tx] = s_NFC[0][s_tx];
	}
}

void colSumKernel(CmdLineArgs& cmdLineArgs, CPUData& cpuData, GPUData& gpuData , bool debug) {

	// transfer Fold
	checkCudaErrors(cudaMemcpy(gpuData.d_Fold, cpuData.h_Fold, gpuData.H_resized * sizeof(float), cudaMemcpyHostToDevice));

	// call kernel
	dColSum<<<dimGrid, dimBlock>>>(gpuData.d_H, gpuData.d_C, gpuData.d_Fold, gpuData.d_col_sum_inter, gpuData.d_debug);
	cudaDeviceSynchronize();

	// retrieve gpuData.d_col_sum_inter
	checkCudaErrors(cudaMemcpy(cpuData.h_col_sum_inter, gpuData.d_col_sum_inter, gpuData.blksY * gpuData.B_resized * sizeof(float), cudaMemcpyDeviceToHost));

}

static __global__ void dRowSum(int* d_B, float* d_C, float* d_P, float* d_col_sum_inter, float* d_row_sum_inter, float* d_debug) {

	// get indecies
	unsigned int s_tx = threadIdx.x;
	unsigned int d_tx = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int d_size_x = gridDim.x * blockDim.x;

	unsigned int s_ty = threadIdx.y;
	unsigned int d_ty = blockDim.y * blockIdx.y + threadIdx.y;

	unsigned int d_x_y_flat = d_ty * d_size_x + d_tx;

	// Work out P for each bin
	float P = 0;
	float denom = d_col_sum_inter[d_tx];

	// @@ 4 ms
	if (denom != 0) {
		P = d_B[d_tx] / denom;
	}
	__syncthreads();

	// Create shared mem CP#define BLOCKSIZE 16
	__shared__ float s_CP[BLOCKSIZE][BLOCKSIZE];

	// @@ 8 ms
	s_CP[s_tx][s_ty] = P * d_C[d_x_y_flat];
	__syncthreads();

	// Sum reduce each column of the CP Transposed - column summation is faster than row summation.
	// @@ 20 ms
	for (unsigned int stride = blockDim.y / 2; stride >= 1; stride >>= 1) {
		__syncthreads();
		if (s_ty < stride) {
			s_CP[s_ty][s_tx] += s_CP[s_ty + stride][s_tx];
		}
	}
	__syncthreads();

	// Save row 0 to global mem - transposed
	// @4ms
	if (s_tx == 0) {
		d_row_sum_inter[d_ty * gridDim.x + blockIdx.x] = s_CP[0][s_ty];
	}

}

void rowSumKernel(CmdLineArgs& cmdLineArgs, CPUData& cpuData, GPUData& gpuData,  bool debug) {

	// transfer cpuData.h_col_sum into first row of gpuData.d_col_sum_inter
	checkCudaErrors(cudaMemcpy(gpuData.d_col_sum_inter, cpuData.h_col_sum, gpuData.B_resized * sizeof(float), cudaMemcpyHostToDevice));

	// call kernel
	dRowSum<<<dimGrid, dimBlock>>>(gpuData.d_B, gpuData.d_C, gpuData.d_P, gpuData.d_col_sum_inter, gpuData.d_row_sum_inter, gpuData.d_debug);
	cudaDeviceSynchronize();

	// retrieve
	checkCudaErrors(cudaMemcpy(cpuData.h_row_sum_inter, gpuData.d_row_sum_inter, gpuData.blksX * gpuData.H_resized * sizeof(float), cudaMemcpyDeviceToHost));
}


static void finalize(CmdLineArgs& cmdLineArgs, CPUData& cpuData, GPUData& gpuData , bool debug) {

	float kT = k_B_KCal * cmdLineArgs.temp;
	float pTotal = 0.0;

	for (int b=0; b< gpuData.B_resized; b++){
		// Work out P for each bin
		float P = 0;
		float denom = cpuData.h_col_sum[b];
		if (denom != 0) {
			P = (float)cpuData.h_B[b] / (float)denom;
		} else {
			P = 0.0;
		}
		cpuData.h_P[b] = P;
		pTotal += P;
	}

	// normalize and get the lowest pmf as an offset
	float min = 1e50;
	for (int b=0; b< gpuData.B_resized; b++){
		cpuData.h_P[b] = cpuData.h_P[b] / pTotal;
		cpuData.h_PMF[b] = -kT * log(cpuData.h_P[b]);
		if (cpuData.h_PMF[b] < min) {
			min = cpuData.h_PMF[b];
		}
	}

	// subtract the minimum to baseline
	for (int b=0; b< gpuData.B_resized; b++){
		cpuData.h_PMF[b] -= min;
	}

	if (debug)
		pprn("P", cpuData.h_P, 1, gpuData.B_resized);

	if (debug)
		pprn("PMF", cpuData.h_PMF, 1, gpuData.B_resized);

	checkCudaErrors(cudaMemcpy(cpuData.h_debug, gpuData.d_debug, gpuData.H_resized * gpuData.B_resized * sizeof(float), cudaMemcpyDeviceToHost));
	if (debug)
		pprn("debug", cpuData.h_debug, gpuData.B_resized, gpuData.H_resized);


	// free those muthas
	checkCudaErrors(cudaFree(gpuData.d_H));
	checkCudaErrors(cudaFree(gpuData.d_B));
	checkCudaErrors(cudaFree(gpuData.d_C));
	checkCudaErrors(cudaFree(gpuData.d_Fold));
	checkCudaErrors(cudaFree(gpuData.d_Fnew));
	checkCudaErrors(cudaFree(gpuData.d_col_sum_inter));
	checkCudaErrors(cudaFree(gpuData.d_row_sum_inter));
	checkCudaErrors(cudaFree(gpuData.d_P));
	checkCudaErrors(cudaFree(gpuData.d_PMF));
	checkCudaErrors(cudaFree(gpuData.d_debug));

	cudaDeviceReset();

}


void sumColumns(float* flattened_array, int y, int x, float* sums) {

	for (int i = 0; i < x; ++i) {
		float col_sum = 0.0;
		for (int j = 0; j < y; ++j) {
			col_sum += flattened_array[j * x + i];
		}
		sums[i] = col_sum;
	}
}

void sumRows(float* flattened_array, int y, int x, float* sums) {

	for (int i = 0; i < y; ++i) {
		float row_sum = 0.0;
		for (int j = 0; j < x; ++j) {
			row_sum += flattened_array[i * x + j];
		}
		sums[i] = row_sum;
	}
}

int converged(CmdLineArgs& cmdLineArgs, CPUData& cpuData, GPUData& gpuData , bool debug) {

	gpuData.no_of_iterations++;
	if (gpuData.no_of_iterations >= cmdLineArgs.max_no_of_iterations) {
		if (LOG) printf("Max number of iterations reached : %d\n", gpuData.no_of_iterations);
		return 1;
	}

	int conv = converged(cmdLineArgs, cpuData, gpuData );

	// bump
	for (int i = 0; i < gpuData.H_resized; i++) {
		cpuData.h_Fold[i] = cpuData.h_Fnew[i];
	}

	if (LOG && (gpuData.no_of_iterations % 100 == 0)) printf("Iteration %d   converged : %d\n", gpuData.no_of_iterations,  conv);

	return conv;
}

void whamGPU(CmdLineArgs& cmdLineArgs, CPUData& cpuData, GPUData& gpuData, WHAMTimes& whamTimes, bool debug){

	initCPU(cmdLineArgs, cpuData, gpuData, debug);
	initGPU(cmdLineArgs, cpuData, gpuData, debug);
	int no_of_iterations=0;

	do {
		// col sum
		colSumKernel(cmdLineArgs, cpuData, gpuData, debug);

		if (debug)
			pprn("col_sum_inter", cpuData.h_col_sum_inter, gpuData.B_resized, gpuData.H_resized / BLOCKSIZE);

		sumColumns(cpuData.h_col_sum_inter, gpuData.H_resized / BLOCKSIZE, gpuData.B_resized, cpuData.h_col_sum);

		if (debug)
			pprn("col_sum", cpuData.h_col_sum, gpuData.B_resized, 1);

		// row sum
		rowSumKernel(cmdLineArgs, cpuData, gpuData, debug);

		if (debug)
			pprn("row_sum_inter", cpuData.h_row_sum_inter, gpuData.B_resized / BLOCKSIZE, gpuData.H_resized);

		sumRows(cpuData.h_row_sum_inter, gpuData.H_resized, gpuData.B_resized / BLOCKSIZE, cpuData.h_Fnew);


		// reciprocal
		for (int i = 0; i < gpuData.H_resized; ++i) {
			if (cpuData.h_Fnew[i] != 0) {
				cpuData.h_Fnew[i] = 1 / cpuData.h_Fnew[i];
			} else {
				cpuData.h_Fnew[i] = 0;
			}
		}

		if (debug)
			pprn("Fnew", cpuData.h_Fnew, 1, gpuData.H_resized);
		no_of_iterations++;

	} while (converged(cmdLineArgs, cpuData, gpuData, debug) < 0);

	// XXX There seems to be 3 variables for gpuData.no_of_iterations
	whamTimes.no_of_iterations = no_of_iterations;

	if (LOG) printf("Number of iterations : %d\n", gpuData.no_of_iterations);

	finalize(cmdLineArgs, cpuData, gpuData, debug);

}

