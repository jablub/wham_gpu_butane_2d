#include <cstdio>
#include <string>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <cstring>
#include <assert.h>

#include "wham-headers.h"

using namespace std;

#define MAX_LINE_SIZE 500

void init(MetaData& metaData, int D, int H){
	metaData.sampleFiles = (char**)malloc(sizeof(char*) * H);
	if(metaData.sampleFiles == NULL) {printf("Error allocating metaData.sampleFiles"); exit(1);}

	metaData.springConstants = (float*)malloc(sizeof(float) * H * D);
	if(metaData.sampleFiles == NULL) {printf("Error allocating metaData.sampleFiles"); exit(1);}

	metaData.umbrellaCenters = (float*)malloc(sizeof(float) * H * D);
	if(metaData.sampleFiles == NULL) {printf("Error allocating metaData.sampleFiles"); exit(1);}
}

// Usage
//  <D> number of dimensions
//  <H> number of simulations
//  <tol> tolerance
//  <temp> temperature
//  <metadata> metadatafile
//  <out> outputfile
//  <d0-min d0-max d0-numBins>...  histogram minimum, maximum and number of bins. 3 values per dimension.
void parseCommandLine(CmdLineArgs& cmdLineArgs, int argc, char **argv){

	// One dimensional is the minimum, so minimum number of arguments apply
	if (argc >= 13) {

		std::istringstream(argv[1]) >> cmdLineArgs.D;
		std::istringstream(argv[2]) >> cmdLineArgs.H;
		std::istringstream(argv[3]) >> cmdLineArgs.tol;
		std::istringstream(argv[4]) >> cmdLineArgs.temp;
		std::istringstream(argv[5]) >> cmdLineArgs.max_no_of_iterations;
		std::istringstream(argv[6]) >> cmdLineArgs.spring_constant_modifier;
		cmdLineArgs.metadataFile = argv[7];
		std::istringstream(argv[8]) >> cmdLineArgs.skipFirst;
		cmdLineArgs.pmfOutDir = argv[9];

		cmdLineArgs.shape = (int*)malloc(cmdLineArgs.D*sizeof(int));
		if(cmdLineArgs.shape == NULL) {printf("Error allocating cmdLineArgs.shape"); exit(1);}
		cmdLineArgs.HBounds = (float*)malloc(cmdLineArgs.D*2*sizeof(float));
		if(cmdLineArgs.HBounds == NULL) {printf("Error allocating cmdLineArgs.HBounds"); exit(1);}
		cmdLineArgs.binWidths = (float*)malloc(cmdLineArgs.D*sizeof(float));
		if(cmdLineArgs.binWidths == NULL) {printf("Error allocating cmdLineArgs.binWidths"); exit(1);}

		cmdLineArgs.B = 1;
		for(int d = 0; d < cmdLineArgs.D; d++){
			int argvBase = 10 + 3 * d;
			std::istringstream(argv[argvBase]) >> cmdLineArgs.HBounds[d * 2];             // histogram min
			std::istringstream(argv[argvBase + 1]) >> cmdLineArgs.HBounds[d * 2 + 1];     // histogram max
			std::istringstream(argv[argvBase + 2]) >> cmdLineArgs.shape[d];               // number of bins
			cmdLineArgs.B = cmdLineArgs.B * cmdLineArgs.shape[d];                         // total number of bins
			cmdLineArgs.binWidths[d] =
					binWidth(cmdLineArgs.HBounds[d * 2], cmdLineArgs.HBounds[d * 2 + 1], cmdLineArgs.shape[d]); // bin width
		}

	} else {
		printf("Not enough parameters.\n");
		exit(-1);
	}

}

void parseMetadataFile(CmdLineArgs& cmdLineArgs, MetaData& metaData){

	if (LOG) printf("Reading : %s\n", cmdLineArgs.metadataFile);

	FILE * metadataFile = fopen(cmdLineArgs.metadataFile, "r");

	if (metadataFile == 0) {
		printf("Error. Cannot read metadata file : %s\n", cmdLineArgs.metadataFile);
		exit(-1);
	}

	int sampleFileNo = 0;
	rewind(metadataFile);

    char linebuffer[MAX_LINE_SIZE];
	char * line;
	int umbrellaCentersCounter = 0;
	int springConstantsCounter = 0;

	// TODO we know the number of histograms so this can be a numeric loop - error if the number of histograms passed in is not correct since it is used to allocate memory. If cmd line H is too low this errors.
	while ((line = fgets(linebuffer, MAX_LINE_SIZE, metadataFile)) != NULL) {


		metaData.sampleFiles[sampleFileNo] = strtostr(line, &line);
		DEBUG && printf("\nsample file: %s ", metaData.sampleFiles[sampleFileNo]);

		for(int d = 0; d < cmdLineArgs.D; d++){
			metaData.umbrellaCenters[umbrellaCentersCounter] = strtod(line, &line);
			DEBUG && printf("%f ", metaData.umbrellaCenters[umbrellaCentersCounter]);
			umbrellaCentersCounter++;
		}

		for(int d = 0; d < cmdLineArgs.D; d++){
			metaData.springConstants[springConstantsCounter] = strtod(line, &line) * cmdLineArgs.spring_constant_modifier;
			DEBUG && printf("%f ", metaData.springConstants[springConstantsCounter]);
			springConstantsCounter++;
		}

		sampleFileNo++;
	}

	fclose (metadataFile);

}
