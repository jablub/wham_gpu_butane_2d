#include <cstdio>
#include <string>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <cstring>
#include <assert.h>

#include "wham-headers.h"

using namespace std;

char* concat(char *s1, char s2[]) {
    char *result = (char*)malloc(strlen(s1)+strlen(s2)+1); //+1 for the zero-terminator
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

void fpprn(char* fileName, float* array, int x, int y) {

	if (LOG) printf("\nWriting : %s\n", fileName);
	FILE * file = fopen(fileName, "w");

	if (file == 0) {
		printf("Error. Cannot write to : %s\n", fileName);
		exit(-1);
	}

	for (int i = 0; i < y; ++i) {
		for (int j = 0; j < x; ++j) {
			fprintf(file, "%.26e", array[i * x + j]);
		}
		fprintf(file, "\n");
	}
	fclose (file);
}

void fpprn(char* fileName, int* array, int x, int y) {

	if (LOG) printf("\nWriting : %s\n", fileName);
	FILE * file = fopen(fileName, "w");

	if (file == 0) {
		printf("Error. Cannot write to : %s\n", fileName);
		exit(-1);
	}

	for (int i = 0; i < y; ++i) {
		for (int j = 0; j < x; ++j) {
			fprintf(file, "%d", array[i * x + j]);
		}
		fprintf(file, "\n");
	}
	fclose (file);
}

void pprn(string name, float* array, int x, int y) {
	if (LOG) printf("\n%s\n", name.c_str());
	for (int i = 0; i < y; ++i) {
		for (int j = 0; j < x; ++j) {
			if (LOG) printf("%10.5f", array[i * x + j]);
		}
		if (LOG) printf("\n");
	}
}

void pprn(string name, int* array, int x, int y) {
	if (LOG) printf("\n%s\n", name.c_str());
	for (int i = 0; i < y; ++i) {
		for (int j = 0; j < x; ++j) {
			if (LOG) printf("%10d", array[i * x + j]);
		}
		if (LOG) printf("\n");
	}
}

//parse

char* strtostr(char *string, char **endPtr) {
	char* p;
	char* str = (char*)calloc(256, sizeof(char));
	int counter = 0;

	p = string;
	// Strip off leading whitespace characters
	while (isspace(*p)) {
		p += 1;
	}

	// take all characters until next whitespace
	while (!isspace(*p)) {
		str[counter] = (char)p[0];
		counter++;
		p += 1;
	}

	if (endPtr != NULL) {
		*endPtr = (char *) p;
	}

	return str;

}

int sum(int* array, int length){
	int i, sum = 0;
	for (i=0; i < length; i++){
		sum += array[i];
	}
	return sum;
}


float binWidth(float min, float max, int numberOfBins){
	return (max - min) / (float)numberOfBins;
}

void printCmdLineArgs(int argc, char **argv){

	for(int i = 0; i < argc; i++){
		if (LOG) printf("%s ", argv[i]);
	}
	if (LOG) printf("\n\n");

	for(int i = 0; i < argc; i++){
		if (LOG) printf("arg[%d] = \"%s\";\n", i, argv[i]);
	}
}


// histograms
void binIndexND(int D, float Hbounds[], float binwidths[], float point[], int binIndexND[]){
	for(int d = 0; d < D; d++){
		binIndexND[d] = (int) ((point[d] - Hbounds[2 * d]) / binwidths[d]);
	}
}

int flattenND(int D, int shape[], int ndIndex[]){
	if (D == 1) {
		return ndIndex[0];
	} else {
		int rlibs = ndIndex[D-1];
		for (int b=0; b < D-1; b++) {
			rlibs = rlibs * shape[b];
		}
		return rlibs + flattenND(D - 1, shape, ndIndex);
	}
}

// TODO dump the results is grossfield format. I have started but not finished since it is too faffy for now.
void dumpResults(GPUData& gpuData, CmdLineArgs& cmdLineArgs, MetaData& metaData){

	char* fileName = concat(cmdLineArgs.pmfOutDir, "wham-gpu-out");
	if (LOG) printf("\nWriting : %s\n", fileName);
	FILE * file = fopen(fileName, "w");

	if (file == 0) {
		printf("Error. Cannot write to : %s\n", fileName);
		exit(-1);
	}

	int binIndexND[cmdLineArgs.D];
	float binCenterND[cmdLineArgs.D];
	for (int b = 0; b < cmdLineArgs.B; b++) {
//		raise(cmdLineArgs.D, cmdLineArgs.shape, b, binIndexND);
//		binCenter(cmdLineArgs.D, cmdLineArgs.HBounds, d_binWidths, binIndexND, binCenterND);
	}
	fclose (file);
}
