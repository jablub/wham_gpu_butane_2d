#include <cstdio>
#include <string>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <cstring>
#include <assert.h>

#include "wham-headers.h"

#include <cuda_runtime.h>
#include <helper_functions.h>
#include <helper_cuda.h>


__device__ void extractNTuple(int base, int N, float source[], float dest[]){
	for(int n = 0; n < N; n++){
		dest[n] = source[base + n];
	}
}

// TODO Figure this out properly one day and write it down in book. Need to write darn unit test first.
// Maybe the results are the same if we loop backwards and indices D-1-d are d
__host__ __device__ void raise(int D, int shape[], int flatIndex, int binIndexND[]){
	for(int d = 0; d < D; d++){
		binIndexND[D-1-d] = flatIndex % shape[D-1-d];
		flatIndex = (int)flatIndex / shape[D-1-d];
	}
}

__host__ __device__ void binCenter(int D, float d_Hbounds[], float binWidths[], int raisedIndex[], float binCenterND[]){
	for(int d = 0; d < D; d++){
		binCenterND[d] = d_Hbounds[2 * d] + raisedIndex[d] * binWidths[d] + 0.5 * binWidths[d];
	}
}

__device__ float hefty(int D, float minusOneOverKT, float K[], float UC[], float binCenterND[]){
	float m, n = 0;
	for(int d = 0; d < D; d++){
		m = (binCenterND[d] - UC[d]);
		n = n + (K[d] * m * m);
	}
	return exp(minusOneOverKT * n);
}

static __global__ void dC(int D, int H, int H_resized, int B, int B_resized, int* d_shape, float* d_Hbounds, float* d_binWidths, float minusOneOverKT, float* d_K, float* d_UC, float* d_C){
	unsigned int d_tx = blockIdx.x *blockDim.x + threadIdx.x;
	unsigned int d_ty = blockIdx.y *blockDim.y + threadIdx.y;

	// calculate the flat index
	int indexC = B_resized * d_ty + d_tx;

	// are we in the data domain
	if (d_tx < B && d_ty < H){

		// calculate the bin center
		// XXX Dynamic array creation in compute capability 2.x+ for now using MAX_DIMS
		float K[MAX_DIMS];
		extractNTuple(D * d_ty, D, d_K, K);
		float UC[MAX_DIMS];
		extractNTuple(D * d_ty, D, d_UC, UC);

		int binIndexND[MAX_DIMS];
		raise(D, d_shape, d_tx, binIndexND);

		float binCenterND[MAX_DIMS];
		binCenter(D, d_Hbounds, d_binWidths, binIndexND, binCenterND);

		d_C[indexC] = hefty(D, minusOneOverKT, K, UC, binCenterND);

	} else {
		// set the value to 0 if in the padded zone
		d_C[indexC] = 0;
	}
	__syncthreads();

}


// The distribution bias equation requires these values
// boltzmann constant
// temperature
// number of dimensions
// spring constants
// umbrella centers
// histogram bounds
// bin widths
void calculateDistributionBiases(GPUData& gpuData, CmdLineArgs& cmdLineArgs, MetaData& metaData, WHAMTimes& whamTimes){

	int D = cmdLineArgs.D;
	int H = cmdLineArgs.H;
	int B = cmdLineArgs.B;

	static int* d_shape;
	checkCudaErrors(cudaMalloc((void ** ) &d_shape, D * sizeof(int)));
	checkCudaErrors(cudaMemcpy(d_shape, cmdLineArgs.shape, D * sizeof(int), cudaMemcpyHostToDevice));

	static float* d_Hbounds;
	checkCudaErrors(cudaMalloc((void ** ) &d_Hbounds, D * 2 * sizeof(float)));
	checkCudaErrors(cudaMemcpy(d_Hbounds, cmdLineArgs.HBounds, D * 2 * sizeof(float), cudaMemcpyHostToDevice));

	static float* d_binWidths;
	checkCudaErrors(cudaMalloc((void ** ) &d_binWidths, D * sizeof(float)));
	checkCudaErrors(cudaMemcpy(d_binWidths, cmdLineArgs.binWidths, D * sizeof(float), cudaMemcpyHostToDevice));

	static float* d_K;
	checkCudaErrors(cudaMalloc((void ** ) &d_K, H *  D * sizeof(float)));
	checkCudaErrors(cudaMemcpy(d_K, metaData.springConstants, H *  D * sizeof(float), cudaMemcpyHostToDevice));

	static float* d_UC;
	checkCudaErrors(cudaMalloc((void ** ) &d_UC, H *  D * sizeof(float)));
	checkCudaErrors(cudaMemcpy(d_UC, metaData.umbrellaCenters, H *  D * sizeof(float), cudaMemcpyHostToDevice));

	checkCudaErrors(cudaMalloc((void ** ) &gpuData.d_C, gpuData.H_resized * gpuData.B_resized * sizeof(float)));

	dim3 dimBlock, dimGrid;
	dimBlock.y = BLOCKSIZE;
	dimBlock.x = BLOCKSIZE;
	dimGrid.y = gpuData.blksY;
	dimGrid.x = gpuData.blksX;
	if (LOG) printf("Grid setup %d x %d blocks :: %d * %d threads \n", dimGrid.y, dimGrid.x, dimBlock.y, dimBlock.x);

	float minusOneOverkT = -1 / (k_B_KCal * cmdLineArgs.temp);

	dC<<<dimGrid, dimBlock>>>(D, H, gpuData.H_resized, B, gpuData.B_resized, d_shape, d_Hbounds, d_binWidths, minusOneOverkT, d_K, d_UC, gpuData.d_C);

	cudaDeviceSynchronize();

	checkCudaErrors(cudaFree(d_shape));
	checkCudaErrors(cudaFree(d_Hbounds));
	checkCudaErrors(cudaFree(d_binWidths));
	checkCudaErrors(cudaFree(d_K));
	checkCudaErrors(cudaFree(d_UC));

}

