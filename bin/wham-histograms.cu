#include <cstdio>
#include <string>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <cstring>
#include <assert.h>

#include "wham-headers.h"

#include <cuda_runtime.h>
#include <helper_functions.h>
#include <helper_cuda.h>

#include <omp.h>

using namespace std;

#define MAX_LINE_SIZE 1000

void init(CPUData& cpuData, int H, int B){
	cpuData.h_H = (int*)calloc (H, sizeof(int));
	cpuData.h_B = (int*)calloc (B, sizeof(int));
}

bool inBounds(CmdLineArgs& cmdLineArgs, float point[]) {
	float nonZeroCheck = 0.0;
	for(int d = 0; d < cmdLineArgs.D; d++){
		if ((point[d] < cmdLineArgs.HBounds[2 * d]) || (point[d] > cmdLineArgs.HBounds[2 * d + 1]) ){
			return false;
		}
		nonZeroCheck += point[d];
	}
	if (nonZeroCheck == 0.0) return false;
	return true;
}

void createHistogramsSerial(CmdLineArgs& cmdLineArgs, MetaData& metaData, CPUData& cpuData){

	char linebuffer[MAX_LINE_SIZE];
	char * points;
	float point[cmdLineArgs.D];
	int binIndex[cmdLineArgs.D];
	int flatIndex;
	int noOfSamples;
	int skip;

	for(int sf = 0; sf < cmdLineArgs.H; sf++){
		if (LOG) printf("Reading sample file: %s", metaData.sampleFiles[sf]);
		noOfSamples = 0;

		FILE * sampleFile = fopen(metaData.sampleFiles[sf], "r");
		if (sampleFile == 0) {
			printf("Error. Cannot read sample file : %s\n", metaData.sampleFiles[sf]);
			exit(-1);
		}

		while ((points = fgets(linebuffer, MAX_LINE_SIZE, sampleFile)) != NULL) {

			// In order to use the same sample files for grossfield we skip the first value which is the time.
			for (skip = 0; skip < cmdLineArgs.skipFirst; skip++) {
				strtod(points, &points);
			}

			for(int column = 0; column < cmdLineArgs.D; column++){
				point[column] = strtod(points, &points);
			}

			if (inBounds(cmdLineArgs, point)){
				binIndexND(cmdLineArgs.D, cmdLineArgs.HBounds, cmdLineArgs.binWidths, point, binIndex);
				flatIndex = flattenND(cmdLineArgs.D, cmdLineArgs.shape, binIndex);
				cpuData.h_B[flatIndex] = cpuData.h_B[flatIndex] + 1;
				noOfSamples++;	omp_set_num_threads(NUM_CPU_THREADS);

			} else {
				DEBUG && printf("Point starting with %f out of bounds.\n", point[0]);
			}
		}

		cpuData.h_H[sf] = noOfSamples;

		if (LOG) printf(" - %d samples\n", noOfSamples);

		fclose (sampleFile);

	}
}


void createHistogramsOpenMP(CmdLineArgs& cmdLineArgs, MetaData& metaData, CPUData& cpuData){

	omp_set_num_threads(NUM_CPU_THREADS);
	if (LOG) printf("Available number of processors %d\n", omp_get_num_procs());
	if (LOG) printf("Available number of threads %d\n", omp_get_max_threads());


	int* threadHistograms = (int*) calloc(cmdLineArgs.B * NUM_CPU_THREADS, sizeof(int));

	int sf;
	int H = cmdLineArgs.H;

#pragma omp parallel
	{
#pragma omp master
		{
			if (LOG) printf("Master thread %d\n", omp_get_thread_num());
			for(sf = 0; sf < cmdLineArgs.H; sf++){
				char* filename = metaData.sampleFiles[sf];

				// this comes from http://www.cplusplus.com/reference/cstdio/fread/
				FILE * sampleFile;
				long fileSize;
				char * buffer;
				size_t result;

				sampleFile = fopen ( filename , "r" );
				if (sampleFile==NULL) {fputs ("File error",stderr); exit (1);}

				// obtain file size:
				fseek (sampleFile , 0 , SEEK_END);
				fileSize = ftell (sampleFile);
				rewind (sampleFile);

				// allocate memory to contain the whole file:
				buffer = (char*) malloc (sizeof(char)*fileSize);
				if (buffer == NULL) {fputs ("Memory error",stderr); exit (2);}

				// copy the file into the buffer:
				result = fread (buffer,1,fileSize,sampleFile);
				if (result != fileSize) {fputs ("Reading error",stderr); exit (3);}


#pragma omp task firstprivate (sf, filename, buffer)
				{
					int threadnumber = omp_get_thread_num();


					float* point = (float*) calloc(cmdLineArgs.D, sizeof(float));
					int* binIndex = (int*) calloc(cmdLineArgs.D, sizeof(int));
					int noOfSamples = 0;


					// This is from http://stackoverflow.com/questions/17983005/c-how-to-read-a-string-line-by-line
					char * points = buffer;
					while(points)
					{
						char * nextLine = strchr(points, '\n');
						if (nextLine) *nextLine = '\0';  // temporarily terminate the current line

						// In order to use the same sample files for grossfield we skip the first value which is the time.
						for (int skip = 0; skip < cmdLineArgs.skipFirst; skip++) {
							strtod(points, &points);
						}

						for(int column = 0; column < cmdLineArgs.D; column++){
							point[column] = strtod(points, &points);
						}

						if (inBounds(cmdLineArgs, point)){
							binIndexND(cmdLineArgs.D, cmdLineArgs.HBounds, cmdLineArgs.binWidths, point, binIndex);
							int flatIndex = flattenND(cmdLineArgs.D, cmdLineArgs.shape, binIndex);
							threadHistograms[(threadnumber * cmdLineArgs.B) + flatIndex] = threadHistograms[(threadnumber * cmdLineArgs.B) + flatIndex] + 1; //threadHistograms[threadnumber * cmdLineArgs.B + flatIndex] + 1;
							noOfSamples++;
						} else {
							DEBUG && printf("Point starting with %f out of bounds.\n", point[0]);
						}

						if (nextLine) *nextLine = '\n';  // then restore newline-char, just to be tidy
						points = nextLine ? (nextLine+1) : NULL;
					}
					cpuData.h_H[sf] = noOfSamples;

					if (LOG) printf("[%d] Read sample file: %s  - %d samples\n", threadnumber, filename, noOfSamples);
					free (buffer);
					free (filename);

				}

				fclose (sampleFile);

			}
#pragma omp taskwait
			if (LOG) printf("Done reading\n");

			// Sum the threadHistograms in parallel... because we can 4343ms to 4210ms
#pragma omp parallel for
			for(int i = 0; i < cmdLineArgs.B; i++){
				int sum = 0;
				for(int j = 0; j < NUM_CPU_THREADS; j++){
					sum += threadHistograms[(j * cmdLineArgs.B) + i];
				}
				cpuData.h_B[i] = sum;
			}

			if (DEBUG) {
				pprn("threadHistograms", threadHistograms, 20, NUM_CPU_THREADS);
			}
			if (LOG) printf("Done summing\n");
			free(threadHistograms);

		}
	}
}

bool testThreadUse() {

	omp_set_num_threads(NUM_CPU_THREADS);
	printf("Available number of processors %d\n", omp_get_num_procs());
	printf("Available number of threads %d\n", omp_get_max_threads());

#pragma omp parallel for
	for(int i = 0; i < NUM_CPU_THREADS; i++){
		int threadnumber = omp_get_thread_num();
		printf("Testing thread number %d\n", threadnumber);
	}


	return bool();
}

void createHistograms(GPUData& gpuData, CmdLineArgs& cmdLineArgs, MetaData& metaData, CPUData& cpuData, WHAMTimes& whamTimes){
	if (DEBUG) {
		printf("H: %d  B: %d\n", cmdLineArgs.H, cmdLineArgs.B);
	}

	if (LOG) {
		testThreadUse();
	}
	createHistogramsOpenMP(cmdLineArgs, metaData, cpuData);
//	createHistogramsSerial(cmdLineArgs, metaData, cpuData);

	if (DEBUG) {
		pprn("B", cpuData.h_B, 10, 1);
		pprn("H", cpuData.h_H, 10, 1);
	}
	checkCudaErrors(cudaMalloc((void ** ) &gpuData.d_H, gpuData.H_resized * sizeof(int)));
	checkCudaErrors(cudaMemset(gpuData.d_H, 0, gpuData.H_resized * sizeof(int)));
	checkCudaErrors(cudaMemcpy(gpuData.d_H, cpuData.h_H, cmdLineArgs.H * sizeof(int), cudaMemcpyHostToDevice));

	checkCudaErrors(cudaMalloc((void ** ) &gpuData.d_B, gpuData.B_resized * sizeof(int)));
	checkCudaErrors(cudaMemset(gpuData.d_B, 0, gpuData.B_resized * sizeof(int)));
	checkCudaErrors(cudaMemcpy(gpuData.d_B, cpuData.h_B, cmdLineArgs.B * sizeof(int), cudaMemcpyHostToDevice));

}

