#include <cstdio>
#include <string>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <sstream>

#include "wham-data.h"
#include "wham-env-defines.h"

using namespace std;

// tests
bool runTests();

// utils
char* concat(char *s1, char s2[]);
void fpprn(char* fileName, float* array, int x, int y);
void fpprn(char* fileName, int* array, int x, int y);
void pprn(string name, float* array, int x, int y);
void pprn(string name, int* array, int x, int y);
char* strtostr(char *string, char **endPtr);
int sum(int* array, int length);

float binWidth(float min, float max, int numberOfBins);
int binIndex1D(float bounds[], float binwidth, float point);
int * binIndexND(int D, float bounds[][2], float binwidths[], float point[]);
void binIndexND(int D, float Hbounds[], float binwidths[], float point[], int binIndexND[]);
int flattenND(int D, int shape[], int ndIndex[]);

// main
void writePMF(CmdLineArgs& cmdLineArgs, CPUData& cpuData);
void finalize(CmdLineArgs& cmdLineArgs, MetaData& metaData, CPUData& cpuData);

// parse
void init(MetaData& metaData, int D, int H);
void init(CPUData& cpuData, int H, int B);
void printCmdLineArgs(int argc, char **argv);
void parseCommandLine(CmdLineArgs& cmdLineArgs, int argc, char **argv);
void parseMetadataFile(CmdLineArgs& cmdLineArgs, MetaData& metaData);

// histograms
void createHistograms(GPUData& gpuData, CmdLineArgs& cmdLineArgs, MetaData& metaData, CPUData& cpuData, WHAMTimes& whamTimes);

// distribution biases
void raise(int D, int shape[], int flatIndex, int binIndexND[]);
void binCenter(int D, float d_Hbounds[], float binWidths[], int raisedIndex[], float binCenterND[]);
void calculateDistributionBiases(GPUData& gpuData, CmdLineArgs& cmdLineArgs, MetaData& metaData, WHAMTimes& whamTimes);

// wham
void init(GPUData& GPUData, int H, int B);
void whamGPU(CmdLineArgs& cmdLineArgs, CPUData& cpuData, GPUData& gpuData, WHAMTimes& whamTimes, bool debug);

// convergence
int converged(CmdLineArgs& cmdLineArgs, CPUData& cpuData, GPUData& gpuData);
